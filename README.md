**This repo is a solution for the following coding challenge:**

<https://github.com/insightful-conduit/coding-assignment-guide/blob/master/backend/backend-v1.md#backend-coding-assignment-v1>


**Project details**

- Node/Express project generated from <https://expressjs.com/en/starter/generator.html> as a starting point.
- Additional packages added: jsonwebtoken, request/request-promise
- Browser support: requires an up to date browser such as Google Chrome that supports some common es6 features: arrow functions, Object.assign
- Files of interest: /routes/api.js
- Limitations: most basic restful api, implements one part of one resource


**Instructions to get the local server running**

1. run npm install
1. run PORT=5218 npm start
1. open http://localhost:5218


**Example**

GET http://localhost:5218/api/v1/inbox/100000

Payload from response

    {
      "payload": [
        {
          "messageSender": "Carl Bosch",
          "messageSenderId": "116ab675f12bee72b7b42997eb8ac194ad563d5e247bd5a90fb88bef25b0d981",
          "messagePriority": 3,
          "messageSubject": "Nullam sodales turpis sit amet tincidunt faucibus.",
          "messageContent": "Tea oporteat corrumpit quo. Sed id ex at mi congue posuere non hendrerit sapien.",
          "messageAt": "2018-12-11T15:19:09.000+00:00",
          "messageId": "37a6d8b50e1aa4c8581e79a28fe5aa370a223e2cea8d1f43cfe3de1709724223"
        },
        {
          "messageSender": "Jean-Baptiste Lamarck",
          "messageSenderId": "2accee32ee5c0592732e1c83cd5de5070136ef24a7151d2db05ec9433c808914",
          "messagePriority": 1,
          "messageSubject": "Capitalize on low hanging fruit to identify a ballpark value added activity to beta test.",
          "messageContent": "Nihil audiam duo ut. Curabitur tincidunt sodales nisi, in scelerisque nulla gravida ut. Virtute nostrum pertinax id per. Quod evertitur est et.",
          "messageAt": "2018-06-30T05:08:55.000+00:00",
          "messageId": "ad467385ef1a98f53a4fd15bd641b87fa00def8bb53ffbe02b86d28c8974406d"
        },
        {
          "messageSender": "Mary Anning",
          "messageSenderId": "a3a4fb06e938165ff4dea2a7d618959676fbfab2770a3dc16025a31f9ab5f163",
          "messagePriority": 1,
          "messageSubject": "Nam a leo sit amet libero ultricies dictum.",
          "messageContent": "Morbi lacinia augue id malesuada malesuada. Donec nec augue nec justo dictum tincidunt. Homero pertinax definitiones at pri. Eu graeci periculis sadipscing vix. Eum nisl dicant doctus ad.",
          "messageAt": "2018-08-08T16:39:34.000+00:00",
          "messageId": "e1946773d2b07e3339fa90958951b72f7ffc04db9da5f86006a166fbf6449c1b"
        }
      ]
    }

const express = require('express');
const router = express.Router();
const request = require('request-promise');
const jwt = require('jsonwebtoken');

const API_PATH = '/v1/';


const sendResponse = (res, statusCode, obj) => {
  return res.status(statusCode).send(obj);
};

// GET /inbox/:inboxId
router.get(API_PATH + 'inbox/:inboxId', (req, res) => {
  if (!validateInboxId(req.params.inboxId)) {
    return sendResponse(res, 404, {
      code: '404',
      status: 'Inbox not found'
    });
  }
  else {
    getInboxTokenAndDecode(req.params.inboxId)
      .then((messages) => {
        const messageList = transformMessagesObj(messages);

        return sendResponse(res, 200, {
          payload: messageList
        });
      })
      .catch((err) => {
        return sendResponse(res, 503, {
          error: err,
          code: '503',
          status: 'Service unavailable'
        });
      });
  }
});

function validateInboxId(inboxId) {
  const parsedIntInboxId = parseInt(inboxId);
  return (!!inboxId &&
    inboxId.length === 6 &&
    !isNaN(parsedIntInboxId) &&
    parsedIntInboxId >= 0 &&
    parsedIntInboxId <= 999999);
}

function getInboxTokenAndDecode(inboxId) {
  return request({
    uri: 'https://coding-assignment-v1.now.sh/api/v1/inbox/' + inboxId,
    json: true
  }).then((data) => {
    return jwt.decode(data.payload);
  }).catch((err) => {
    return err;
  });
}

function transformMessagesObj(messages) {
  const sortByPriority = (list) => {
    return list.sort((a, b) => b.messagePriority - a.messagePriority);
  };

  return sortByPriority(Object.keys(messages.messages).map(id => (
    Object.assign({}, messages.messages[id], {messageId: id})
  )));
}

module.exports = router;
